public class Ikb.Util.Intparser {
	public static int? parse_integer(string number, int? default_value) {
		int result = 0;
		if (int.try_parse(number, out result)) {
			return result;
		} else {
			return default_value;
		}
	}
}
