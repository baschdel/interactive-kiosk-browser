public interface Ikb.Interface.Browser : Object {
	public signal void request_navigation_to(string uri); //user clicked a link
	public signal void load_started(string uri);
	public signal void load_redirect(string uri);
	public signal void load_finished(string uri);
	public signal void load_error(string uri, string error, string message);
	public signal void on_zoom_level_changed(double zoom_level);
	
	public abstract void add_cookie(Soup.Cookie cookie);
	
	public abstract string get_title();
	
	public abstract void navigate_to(string uri);
	public abstract string get_current_uri();
	
	public abstract bool can_go_back();
	public abstract bool can_go_forward();
	
	public abstract void go_back();
	public abstract void go_forward();
	
	public abstract void set_zoom_level(double zoom_level);
	public abstract double get_zoom_level();
}
