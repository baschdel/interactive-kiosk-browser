namespace Ikb.GlobalCssLoader {
	
	//Returns null on success
	public static string? add_global_css_provider(string filepath) {
		try {
			var css_provider = new Gtk.CssProvider();
			css_provider.load_from_path(filepath);
			Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(), css_provider, Gtk.STYLE_PROVIDER_PRIORITY_USER);
			return null;
		} catch (Error e) {
			return e.message;
		}
	}
}
