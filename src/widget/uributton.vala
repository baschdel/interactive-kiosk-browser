public class Ikb.Widget.UriButton : Gtk.Button {
	
	private Ikb.Interface.Browser browser;
	private string uri;
	private string label_template;
	
	public UriButton(Ikb.Interface.Browser browser, string uri, string label, string icon_name, int? custom_icon_size = null){
		this.browser = browser;
		this.label_template = label;
		update_label();
		this.uri = uri;
		this.get_style_context().add_class("uri-button");
		this.on_load_update("");
		this.browser.load_finished.connect(this.on_load_update);
		this.browser.load_error.connect(this.on_load_update);
		this.browser.load_started.connect(this.on_load_update);
		if (icon_name != "") {
			Gtk.Image image;
			if (icon_name.contains("/")) {
				if (custom_icon_size == null) {
					image = new Gtk.Image.from_file(icon_name);
				} else {
					try {
						var pixbuf = new Gdk.Pixbuf.from_file_at_scale(icon_name, -1, custom_icon_size, true);
						image = new Gtk.Image.from_pixbuf(pixbuf);
					} catch (Error e) {
						warning(e.message);
						image = new Gtk.Image.from_icon_name(icon_name, BUTTON);
					}
				}
			} else {
				image = new Gtk.Image.from_icon_name(icon_name, BUTTON);				
				if (custom_icon_size != null) {
					image.icon_size = custom_icon_size;
				}
			}
			this.image = image;
			this.always_show_image = true;
			this.image_position = LEFT;
		}
		if (label_template.contains("{{{clock:")) {
			Timeout.add(5000, () => {
				update_label();
				return true;
			});
		}
	}
	
	private void update_label(){
		if (label_template.contains("{{{clock:")) {
			var now = new GLib.DateTime.now_local();
			string temp = label_template.replace("{{{clock:year}}}", @"$(now.get_year())");
			temp = temp.replace("{{{clock:month}}}", "%02d".printf(now.get_month()));
			temp = temp.replace("{{{clock:day}}}", "%02d".printf(now.get_day_of_month()));
			temp = temp.replace("{{{clock:hour}}}", "%02d".printf(now.get_hour()));
			this.label = temp.replace("{{{clock:minute}}}", "%02d".printf(now.get_minute()));
		} else {
			this.label = label_template;
		}
	}
	
	private void on_load_update(string uri){
		if (this.uri == "about:back") {
			this.sensitive = browser.can_go_back();
		} else if (this.uri.contains("{{{uri}}}")) {
			string[] split = this.uri.split("{{{uri}}}",2);
			if (split.length == 2) {
				this.sensitive = !(uri.has_prefix(split[0]) && uri.has_suffix(split[1]));
			} else {
				this.sensitive = true;
			}
		} else {
			this.sensitive = uri != this.uri;
		}
	}
	
	public override void clicked(){
		if (uri != "about:here") {
			if (uri.contains("{{{uri}}}")) {
				string escaped_uri = GLib.Uri.escape_string(browser.get_current_uri(), null, false);
				browser.navigate_to(uri.replace("{{{uri}}}", escaped_uri));
			} else if (uri == "about:back") {
				browser.go_back();
			} else {
				browser.navigate_to(uri);
			}
		}
	}
	
}
