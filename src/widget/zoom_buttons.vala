public class Ikb.Widget.ZoomButtons : Gtk.Box {

	private Ikb.Interface.Browser browser;
	private Gtk.Button zoom_in_button = new Gtk.Button.from_icon_name("zoom-in-symbolic");
	private Gtk.Button zoom_out_button = new Gtk.Button.from_icon_name("zoom-out-symbolic");
	private Gtk.Label zoom_label = new Gtk.Label("100%");
	
	public double min_zoom_level = 0.7;
	public double max_zoom_level = 2.5;
	public double zoom_step = 0.1;
	
	public ZoomButtons(Ikb.Interface.Browser browser, int? custom_icon_size = null){
		this.browser = browser;
		this.orientation = HORIZONTAL;
		this.pack_start(zoom_out_button, false, false);
		this.pack_start(zoom_label);
		this.pack_start(zoom_in_button, false, false);
		this.zoom_in_button.clicked.connect(zoom_in);
		this.zoom_out_button.clicked.connect(zoom_out);
		this.zoom_label.width_chars = 6;
		this.browser.on_zoom_level_changed.connect(update_zoom_level);
		update_zoom_level(this.browser.get_zoom_level());
		this.get_style_context().add_class("zoom-buttons");
		if (custom_icon_size != null) {
			set_button_icon_size(zoom_in_button, custom_icon_size);
			set_button_icon_size(zoom_out_button, custom_icon_size);
		}
	}
	
	private void set_button_icon_size(Gtk.Button button, int size){
		var image = button.image as Gtk.Image;
		if (image != null) {
			image.icon_size = size;
		}
	}
	
	private void zoom_in(){
		double new_zoom_level = browser.get_zoom_level()+zoom_step;
		if (new_zoom_level > max_zoom_level) {
			new_zoom_level = max_zoom_level;
		}
		browser.set_zoom_level(new_zoom_level);
	}
	
	private void zoom_out(){
		double new_zoom_level = browser.get_zoom_level()-zoom_step;
		if (new_zoom_level < min_zoom_level) {
			new_zoom_level = min_zoom_level;
		}
		browser.set_zoom_level(new_zoom_level);
	}
	
	private void update_zoom_level(double zoom_level){
		int zoom = (int)(zoom_level*100);
		zoom_label.label = @"$zoom%";
		zoom_in_button.sensitive = zoom_level < max_zoom_level;
		zoom_out_button.sensitive = zoom_level > min_zoom_level;
	}
}
