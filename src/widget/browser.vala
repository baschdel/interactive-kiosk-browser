public class Ikb.Browser : Gtk.ScrolledWindow, Ikb.Interface.Browser {
	
	public WebKit.WebView web_view = new WebKit.WebView();
	
	private Gtk.GestureDrag drag_gesture;
	private double drag_start_x = 0;
	private double drag_start_y = 0;
	
	public Browser() {
		web_view.load_changed.connect(on_load_changed);
		web_view.load_failed.connect(on_load_failed);
		web_view.decide_policy.connect(on_decide_policy);
		add(web_view);
		web_view.show();
		this.drag_gesture = new Gtk.GestureDrag(this);
		drag_gesture.drag_begin.connect(on_drag_start);
		drag_gesture.drag_update.connect(on_drag_update);
	}
	
	private void on_drag_start(double pos_x, double pos_y){
		drag_start_x = this.hadjustment.value;
		drag_start_y = this.vadjustment.value;
	}
	
	private void on_drag_update(double offset_x, double offset_y){
		this.hadjustment.value = drag_start_x - offset_x;
		this.vadjustment.value = drag_start_y - offset_y;
	}
	
	private bool on_load_failed(WebKit.WebView source, WebKit.LoadEvent load_event, string failing_uri, Error error){
		warning(@"Load failed: $(error.domain):$(error.code) - $(error.message)\n");
		load_error(failing_uri, @"$(error.domain):$(error.code)", error.message);
		return true;
	}
	
	private void on_load_changed(WebKit.LoadEvent load_event){
		//print(@"Load changed: $load_event - $(web_view.uri)\n");
		switch (load_event) {
			case STARTED:
				load_started(web_view.uri);
				break;
			case REDIRECTED:
				load_redirect(web_view.uri);
				break;
			case FINISHED:
				load_finished(web_view.uri);
				break;
			case COMMITTED:
			default:
				return;
		};
	}
	
	private bool on_decide_policy(WebKit.PolicyDecision decision, WebKit.PolicyDecisionType type){
		var navigation_decision = (decision as WebKit.NavigationPolicyDecision);
		if (navigation_decision != null) {
			if (navigation_decision.navigation_action.is_user_gesture()){
				decision.ignore();
				request_navigation_to(navigation_decision.navigation_action.get_request().uri);
				return true;
			}
		}
		return false;
	}
	
	  ///////////////////////////
	 // Ikb.Interface.Browser //
	///////////////////////////
	
	public void add_cookie(Soup.Cookie cookie){
		web_view.web_context.get_cookie_manager().add_cookie.begin(cookie);
	}
	
	public string get_title(){
		return web_view.title;
	}
	
	public void navigate_to(string uri){
		web_view.load_uri(uri);
	}
	
	public string get_current_uri(){
		return web_view.uri;
	}
	
	public bool can_go_back(){
		return web_view.can_go_back();
	}
	
	public bool can_go_forward(){
		return web_view.can_go_forward();
	}
	
	public void go_back(){
		web_view.go_back();
	}
	
	public void go_forward(){
		web_view.go_forward();
	}
	
	public void set_zoom_level(double zoom_level){
		web_view.zoom_level = zoom_level;
		on_zoom_level_changed(zoom_level);
	}
	
	public double get_zoom_level(){
		return web_view.zoom_level;
	}
	
}
