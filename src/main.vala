public static void message(string message) {
	stderr.write((message+"\n").data);
}

public static int main(string[] args) {
	
	Gtk.init(ref args);
	
	bool next_is_allowed_domainname = false;
	bool next_is_homepage = false;
	bool next_is_fallback_uri = false;
	bool next_is_navbutton_uri = false;
	bool next_is_nav_icon = false;
	bool next_is_nav_class = false;
	bool next_nav_is_special = false;
	string? next_navbutton_uri = null;
	bool next_is_cookie_name = false;
	string? next_cookie_name = null;
	string? next_cookie_value = null;
	string? next_cookie_domain = null;
	string? next_cookie_path = null;
	bool next_is_css_file = false;
	bool next_is_custom_icon_size = false;
	int? custom_icon_size = null;
	bool print_help = false;
	string? firstarg = null;
	string? homepage = null;
	string? fallback_uri = null;
	
	string[] navbutton_uris = {};
	string[] navbutton_labels = {};
	bool[] navbutton_specials = {};
	string[] navbutton_icons = {};
	string[] navbutton_classes = {};
	
	string[] allowed_domains = {};
	
	Soup.Cookie[] cookies = {};
	
	foreach(string arg in args) {
		if (firstarg == null) {
			firstarg = arg;
		} else if (next_is_homepage) {
			homepage = arg;
			next_is_homepage = false;
		} else if (next_is_navbutton_uri) {
			next_navbutton_uri = arg;
			if (homepage == null && !(arg.has_prefix("about:"))) {
				homepage = arg;
			}
			next_is_navbutton_uri = false;
		} else if (next_navbutton_uri != null) {
			navbutton_uris += next_navbutton_uri;
			navbutton_labels += arg;
			navbutton_specials += next_nav_is_special;
			navbutton_icons += "";
			navbutton_classes += "";
			next_navbutton_uri = null;
		} else if (next_is_nav_icon) {
			if (navbutton_icons.length > 0) {
				navbutton_icons[navbutton_icons.length-1] = arg;
			}
			next_is_nav_icon = false;
		} else if (next_is_nav_class) {
			if (navbutton_classes.length > 0) {
				navbutton_classes[navbutton_classes.length-1] = navbutton_classes[navbutton_classes.length-1]+arg+" ";
			}
			next_is_nav_class = false;
		} else if (next_cookie_path != null) {
			int max_age = Ikb.Util.Intparser.parse_integer(arg, -1);
			cookies += new Soup.Cookie(next_cookie_name, next_cookie_value, next_cookie_domain, next_cookie_path, max_age);
			next_cookie_name = null;
			next_cookie_value = null;
			next_cookie_domain = null;
			next_cookie_path = null;
		} else if (next_cookie_domain != null) {
			next_cookie_path = arg;
		} else if (next_cookie_value != null) {
			next_cookie_domain = arg;
		} else if (next_cookie_name != null) {
			next_cookie_value = arg;
		} else if (next_is_cookie_name) {
			next_cookie_name = arg;
			next_is_cookie_name = false;
		} else if (next_is_allowed_domainname) {
			allowed_domains += arg;
			next_is_allowed_domainname = false;
		} else if (next_is_fallback_uri) {
			fallback_uri = arg;
			next_is_fallback_uri = false;
		} else if (next_is_css_file) {
			message(@"[css] Loading css file: $arg\n");
			var error = Ikb.GlobalCssLoader.add_global_css_provider(arg);
			if (error != null) {
				message(@"[css] Error while loading css file:\n $error\n");
			}
			next_is_css_file = false;
		} else if (next_is_custom_icon_size) {
			custom_icon_size = Ikb.Util.Intparser.parse_integer(arg, null);
			next_is_custom_icon_size = false;
		} else {
			switch (arg) {
				case "--home":
					next_is_homepage = true;
					break;
				case "--css":
					next_is_css_file = true;
					break;
				case "--icon-size":
					next_is_custom_icon_size = true;
					break;
				case "--nav-special":
				case "--nav":
					next_nav_is_special = (arg == "--nav-special");
					next_is_navbutton_uri = true;
					break;
				case "--nav-icon":
					next_is_nav_icon = true;
					break;
				case "--nav-add-class":
					next_is_nav_class = true;
					break;
				case "--add-cookie":
					next_is_cookie_name = true;
					break;
				case "--allow-host":
					next_is_allowed_domainname = true;
					break;
				case "--fallback":
					next_is_fallback_uri = true;
					break;
				default:
					message(@"Error while progesing argument: $arg");
					print_help = true;
					break;
			}
		}
	}
	
	if (print_help) {
		message(@"Usage: $firstarg
A browser that can be used in interactive information kiosks.

      --home          the next argument is the uri of the homepage
      --css           the next argument is a path to a css file (gtk css)
      --icon-size     the icon height in pixels for all toolbar icons
      --nav           the next two arguments define the uri and label of
                      a navigation button in the toolbar.
      --nav-special   like nav but on the other side of the toolbar.
      --nav-icon      Filepath to the icon to be added to the last defined nav.
      --nav-add-class Add a css class to the last specified navbutton.
      --add-cookie    <name> <value> <domain> <path> <max_age>
                      Set max_age to -1 to keep the cookie from expiring.
      --allow-host    Adds a hostname/domainname to the allowlist.
      --fallback      Adds a fallback uri, {{{uri}}} will be replaced with
                      an urlencoded version of the curent uri, for when a
                      page is not allwed to be visited, not setting results
                      in the browser ignoring the navigation request.
      
\n");
		return 0;
	}
	
	 ///////////////////////////////////////
	// Initialize UI
	
	var window = new Gtk.Window();
	window.destroy.connect(Gtk.main_quit);
	
	var main_box = new Gtk.Box(VERTICAL, 0);
	window.add(main_box);
	
	var toolbar_box = new Gtk.Box(HORIZONTAL, 0);
	main_box.pack_start(toolbar_box, false, false);
	toolbar_box.get_style_context().add_class("toolbar-box");
	
	var browser = new Ikb.Browser();
	main_box.pack_start(browser, true, true);
	browser.valign = FILL;
	
	foreach (var cookie in cookies) {
		browser.add_cookie(cookie);
	}
	
	var zoom_widget = new Ikb.Widget.ZoomButtons(browser, custom_icon_size);
	toolbar_box.pack_end(zoom_widget, false, false);

	if (homepage != null) {
		browser.navigate_to(homepage);
	}
	
	for (int i=0; i<navbutton_uris.length; i++) {
		string uri = navbutton_uris[i];
		string label = navbutton_labels[i];
		bool is_special = navbutton_specials[i];
		string icon = navbutton_icons[i];
		string[] classes = navbutton_classes[i].split(" ");
		var uri_button = new Ikb.Widget.UriButton(browser, uri, label, icon, custom_icon_size);
		foreach (string clazz in classes) {
			if (clazz != "") {
				uri_button.get_style_context().add_class(clazz);
			}
		}
		if (is_special) {
			toolbar_box.pack_end(uri_button, false, false);
		} else {
			toolbar_box.pack_start(uri_button, false, false);
		}
	}
	
	browser.request_navigation_to.connect((uri) => {
		message(@"Navigation request: $uri");
		if (uri == "about:home" && homepage != null) {
			browser.navigate_to(homepage);
		}
		bool allow = false;
		if (uri.has_suffix(".pdf")) {
			allow = false; //use fallback for pdfs
		} else if (uri == homepage) {
			allow = true;
		} else if (uri in navbutton_uris) {
			allow = true;
		} else {
			var _uri = new Slate.Util.Uri(uri);
			allow = (_uri.hostname in allowed_domains);
		}
		if (allow) {
			browser.navigate_to(uri);
		} else {
			string escaped_uri = GLib.Uri.escape_string(uri, null, false);
			browser.navigate_to(fallback_uri.replace("{{{uri}}}", escaped_uri));
		}
	});
	
	window.show_all();
	Gtk.main();
	return 0;
}
