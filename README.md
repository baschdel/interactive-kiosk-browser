# interactive-kiosk-browser

A browser that can be used as part of an interactive information kiosk.

## Configuration

The interactive-kiosk-browser can currently be configured using command line options:

```
--home          the next argument is the uri of the homepage
--css           the next argument is a path to a css file (gtk css)
--icon-size     the icon height in pixels for all toolbar icons
--nav           the next two arguments define the uri and label of
                a navigation button in the toolbar.
--nav-special   like nav but on the other side of the toolbar.
--nav-icon      Filepath to the icon to be added to the last defined nav.
--nav-add-class Add a css class to the last specified navbutton.
--add-cookie    <name> <value> <domain> <path> <max_age>
                Set max_age to -1 to keep the cookie from expiring.
--allow-host    Adds a hostname/domainname to the allowlist.
--fallback      Adds a fallback uri, {{{uri}}} will be replaced with
                an urlencoded version of the curent uri, for when a
                page is not allwed to be visited, not setting results
                in the browser ignoring the navigation request.
```

### Special uris:

- about:here - Do nothing
- about:home - Go to the page that is configured as homepage
- about:back - Go back

### Uri templating:

Use `{{{uri}}}` unside an uri for a navbutton to replace it with an escaped version of the current uri or in case of the fallback uri, the uri that was not allowed to load.

### Name Templating

You can add a clock and date button by including the following in a navbuttons name:
- `{{{clock:minute}}}`
- `{{{clock:hour}}}`
- `{{{clock:day}}}`
- `{{{clock:month}}}`
- `{{{clock:year}}}`

### Styling

Using the `--css` switch you can load one or more stylesheets to style the button bar, since this application is written in gtk you can use the gtk debugger to live edit the css and to inspect which elements have which classes.

The top button bar has the class `toolbar-box` the zoom buttons on the right have the the class `zoom-buttons`. All nav elements have the class `uri-button` (and the additional classes you specified).

The the currently displayed or loading uri equals (or passes a prefix suffix match in case you use templating) the uri the button is for the button gets deactivated, in case of the about:back uri the program tests if the webview can go to a previous page.

You can specify the icon-size using the `--icon-size` switch in pixels for the whole button bar, this will cause all loaded icons to be resized to be $icon-size pixels hight and keep their aspect ratio.

### Allowed hosts

The browser has an allowlist with uris and hosts, The user is allowed to visit an uri if:
- it is the uri specified as one of the navigation buttons.
- it is one of the special uris from above
- the hostname in the uri is specified as an `--allowed-host`

## How to build?

### Dependencies
- A working vala compiler
- glib-2.0
- meson
- gtk+-3.0
- webkit2gtk-4.0

If you wannt to use all the scripts (not just `build` and `run`) you also need lua 5.x installed

A guaranteed up to date list of dependencies can be found in the src/meson.build file.

If you added or removed source files use the `update_src_build_files` script, it will atomatically update the src/meson.build file so you don't have to do anything.

### Building and running
To build it, run the `build.sh` script, which will automatically setup
the build folder, run ninja, and put the output in the projects root
directory. The produced binary should be executable, now.

To make development easy, the `run` script calls the `build.sh` script
and then runs whatever is at the output (it will try and fail if there is none).
